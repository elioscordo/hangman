from django.views import View
from django.shortcuts import render
from django.shortcuts import redirect

from hangman.models import Game
from hangman.forms import ActionForm


class CreateGame(View):
    
    def get(self, request):
        return render(request,'create.html')

    def post(self, request):
        game = Game.objects.create(word="Football")
        return redirect("/hangman/play/{}".format(game.id))


class PlayGame(View):

    def get(self, request,id):
        game = Game.objects.get(id=id)
        return render(request,'play.html',{"game":game,"form":ActionForm()})

    def post(self, request,id):
        game = Game.objects.get(id=id)
        form = ActionForm(request.POST)
        if form.is_valid() and not game.is_ended():
            action = form.save(commit=False)
            action.game = game
            action.save()
        return render(request,'play.html', context={"game":game,"form":form})
