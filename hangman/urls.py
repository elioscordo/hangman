from django.conf.urls import url
from hangman.views import *

urlpatterns = [
    url(r'^create$', CreateGame.as_view()),
    url(r'^play/(?P<id>\d+)$', PlayGame.as_view()),
]
