from django.forms import ModelForm,ValidationError
from hangman.models import Action


class ActionForm(ModelForm):

    def clean(self):
        # Then call the clean() method of the super  class
        cd = super(ModelForm, self).clean()
        if cd["type"] == "help" and len(cd["input"]) != 1:
            raise ValidationError("Just on letter please")
        return cd

    class Meta:
        model = Action
        fields = ['type', 'input']
