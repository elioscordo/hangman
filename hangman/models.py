# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils.translation import ugettext_lazy as _

MAX_ACTIONS = 9


class Letter:
    def __init__(self,char,hidden):
        self.char = char
        self.hidden = hidden


class Game(models.Model):
    word = models.CharField(_('Word'), max_length=200)
    state = None
    letters = None

    def is_ended(self):
        return self.action_set.filter(game=self).count() >= MAX_ACTIONS or self.is_won()

    def get_letters(self):
        if not self.letters:
            inputs = []
            self.letters = []
            for action in self.action_set.filter(type='help'):
                inputs.append(action.input)
            for char in self.word:
                is_hidded = char not in inputs
                self.letters.append(Letter(char, is_hidded))
        return self.letters

    def is_won(self):
        return self.action_set.filter(type='solution', input=self.word).exists()


class Action(models.Model):
    TYPE_CHOICES = (("solution", _("Solution")),
                    ("help", _("Help")),
                    )
    type = models.CharField(_("Type"), default="help", max_length=200, choices=TYPE_CHOICES)
    input = models.CharField(_('Input'), max_length=200)
    game = models.ForeignKey(Game)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['created',]
