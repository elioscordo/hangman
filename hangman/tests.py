import datetime

from rest_framework.test import APITestCase

from hangman.models import *


class GameCase(APITestCase):

    def setUp(self):
        self.tile1 = Game.objects.create(word="football")
        self.actions = [
            Action.objects.create(type="help", input="o"),
            Action.objects.create(type="help", input="a"),
            Action.objects.create(type="help", input="a"),
        ]


