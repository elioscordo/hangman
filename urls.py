from django.conf.urls import url, include

urlpatterns = [
    url(r'^hangman/', include('hangman.urls')),
]
